/*
 * About template
 * By Joan CiberSheep using base file from uNav
 *
 * uNav is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * uNav is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

import "Components"

Page {
    id: aboutPage
    objectName: 'aboutPage'

    //Margins
    property int marginColumn: units.gu(1)

    //Properties
    property string iconAppRute: "../assets/logo.svg"
    property string version: "@APP_VERSION@"
    property string license: "<a href='https://www.gnu.org/licenses/gpl-3.0.html'>GPLv3</a>"
    property string source: "<a href='https://gitlab.com/cibersheep/tfamanager'>Gitlab</a>"
    property string appName: "@APP_NAME@"

    header: HeaderBase {
        id: pageHeader
        title: i18n.tr("About")
        flickable: aboutView
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: aboutView
    }

    ListView {
        id: aboutView
        section.property: "category"
        section.criteria: ViewSection.FullString

        section.delegate: ListItemHeader {
            title: section
        }

        anchors {
            fill: parent
            topMargin: units.gu(5)
        }

        header: Column {
            width: parent.width - units.gu(6)
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: units.gu(3)

            LomiriShape {
                width: units.gu(20)
                height: width
                aspect: LomiriShape.Flat
                anchors.horizontalCenter: parent.horizontalCenter

                source: Image {
                    sourceSize.width: parent.width
                    sourceSize.height: parent.height
                    source: iconAppRute
                }
            }

            Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr(appName)
                font.bold: true
            }

            Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Version %1. Source %2").arg(version).arg(source)
                onLinkActivated: Qt.openUrlExternally(link)
                linkColor: mainColor
            }

            Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Under License %1").arg(license)
                onLinkActivated: Qt.openUrlExternally(link)
                linkColor: mainColor
            }

            Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("App used %1 times. Consider donating: %2").arg(appsettins.runs).arg("<a href='https://liberapay.com/cibersheep/donate'>liberapay.com/cibersheep</a>")
                onLinkActivated: Qt.openUrlExternally(link)
                linkColor: mainColor
            }

            Item {
                width: parent.width
                height: units.gu(2)
            }
        }

        model: aboutModel

        delegate: ListItem {
            height: storiesDelegateLayout.height
            divider.visible: false
            highlightColor: highlightColor

            ListItemLayout {
                id: storiesDelegateLayout
                title.text: mainText
                subtitle.text: secondaryText

                ProgressionSlot {
                    name: link !== "" ? "next" : ""
                }
            }

            onClicked: model.link !== "" ? Qt.openUrlExternally(model.link) : null
        }

        ListModel {
            id: aboutModel

            Component.onCompleted: initialize()

            function initialize() {
                aboutModel.append([
                    {   category: i18n.tr("App Development"),
                        mainText: "Joan CiberSheep",
                        secondaryText: "",
                        link: "https://cibersheep.com/"
                    },


                    {   category: i18n.tr("Translate the app"),
                        mainText: "gitlab.com",
                        secondaryText: "",
                        link: "https://gitlab.com/cibersheep/tfamanager/-/blob/master/po/tfamanager.cibersheep.pot"
                    },

                    {   category: i18n.tr("Code Used from"),
                        mainText: "Héctor Molinero Fernández",
                        secondaryText: "OTPAuth (MIT)",
                        link: "https://gitlab.com/hectorm/otpauth"
                    },

                    {   category: i18n.tr("Icons"),
                        mainText: i18n.tr("App Icon"),
                        secondaryText: "CC-By Evgeny Filatov",
                        link: "https://thenounproject.com/efilatov/"
                    },

                    {   category: i18n.tr("Icons"),
                        mainText: i18n.tr("Scan Icon"),
                        secondaryText: "CC-By Tienan",
                        link: "https://thenounproject.com/zhoutienan/"
                    }
                ])
            }
        }
    }
}

