# This file is part of the 2FA Manager App for Ubuntu Touch
# Copyright (C) 2019 Joan CiberSheep
# This file is distributed under the same license as the tfamanager.cibersheep package.
# Joan CiberSheep <info@cibersheep.com> (C) 2019
#
msgid ""
msgstr ""
"Project-Id-Version: tfamanager.cibersheep\n"
"Report-Msgid-Bugs-To: info@cibersheep.com\n"
"POT-Creation-Date: 2024-12-03 21:39+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../About.qml.in:37
msgid "About"
msgstr ""

#: ../About.qml.in:91
msgid "Version %1. Source %2"
msgstr ""

#: ../About.qml.in:100
msgid "Under License %1"
msgstr ""

#: ../About.qml.in:109
msgid "App used %1 times. Consider donating: %2"
msgstr ""

#: ../About.qml.in:147
msgid "App Development"
msgstr ""

#: ../About.qml.in:154
msgid "Translate the app"
msgstr ""

#: ../About.qml.in:160
msgid "Code Used from"
msgstr ""

#: ../About.qml.in:166 ../About.qml.in:172
msgid "Icons"
msgstr ""

#: ../About.qml.in:167
msgid "App Icon"
msgstr ""

#: ../About.qml.in:173
msgid "Scan Icon"
msgstr ""

#: ../qml/Components/ChooserTheme.qml:49
msgid "System Theme"
msgstr ""

#: ../qml/Components/DialogDonate.qml:27
msgid "Consider donating"
msgstr ""

#: ../qml/Components/DialogDonate.qml:28
msgid "You have been using this app %1 times."
msgstr ""

#: ../qml/Components/DialogDonate.qml:31
msgid "Donate"
msgstr ""

#: ../qml/Components/DialogDonate.qml:41
#: ../qml/Components/DialogImportResum.qml:32
#: ../qml/Components/HeaderMain.qml:62
msgid "Close"
msgstr ""

#: ../qml/Components/DialogImportResum.qml:7
msgid "Import Summary"
msgstr ""

#: ../qml/Components/DialogImportResum.qml:14
msgid "Keys imported successfully: "
msgstr ""

#: ../qml/Components/DialogImportResum.qml:20
msgid "Keys skipped: "
msgstr ""

#: ../qml/Components/DialogImportResum.qml:26
msgid "Keys with errors: "
msgstr ""

#: ../qml/Components/EmptyDocument.qml:34
msgid "Empty List"
msgstr ""

#: ../qml/Components/EmptyDocument.qml:35
msgid ""
"Please, scan a Qr code with Barcode Reader app and tap 'Open url' or add "
"info manually in the bottom edge menu"
msgstr ""

#: ../qml/Components/HeaderInfo.qml:39 ../qml/Components/PageEditAddKey.qml:42
msgid "Edit"
msgstr ""

#: ../qml/Components/HeaderMain.qml:53
msgid "Search key…"
msgstr ""

#: ../qml/Components/HeaderMain.qml:71 ../qml/Components/PageSettings.qml:33
msgid "Settings"
msgstr ""

#: ../qml/Components/HeaderMain.qml:83
msgid "Search"
msgstr ""

#: ../qml/Components/HeaderSettings.qml:27
msgid "Information"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:61
msgid "Description"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:68 ../qml/Components/RenderUriPopUp.qml:38
msgid "Add a description"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:80
msgid "Paste an <i>otpauth://</i> URI"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:95
msgid "Paste URI"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:102 ../qml/Components/PageEditAddKey.qml:43
#: ../qml/Components/RenderUriPopUp.qml:86
msgid "Add"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:118
msgid "Or type the information"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:133
msgid "Type"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:161
msgid "<b>Issuer</b>"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:168
msgid "Who is providing the service"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:185 ../qml/Components/RenderUriPopUp.qml:48
msgid "User"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:192
msgid "Who is using the service"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:213 ../qml/Components/RenderUriPopUp.qml:53
msgid "Algorithm"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:242 ../qml/Components/RenderUriPopUp.qml:61
msgid "Password Length"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:273 ../qml/Components/RenderUriPopUp.qml:68
msgid "Interval"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:281
msgid "Password life in seconds"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:300 ../qml/Components/RenderUriPopUp.qml:74
msgid "Counter"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:307
msgid "Password initial generation"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:318 ../qml/Components/PageCheckdB.qml:188
#: ../qml/Components/PageImport.qml:188 ../qml/Components/PageKeysInfo.qml:230
msgid "Secret Key"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:325
msgid "Secret key base"
msgstr ""

#: ../qml/Components/KeyToEdit.qml:372
msgid ""
"URI format is invalid. The expected format is 'otpauth://totp/[ID]?"
"secret=[CODE]`"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:51
msgid "Delete"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:73
msgid "Copy key"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:80
msgid "Edit key"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:104
msgid "%1 at %2"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:105
msgid "%1. %2 at %3"
msgstr ""

#: ../qml/Components/KeysDelegate.qml:115
msgid "# %1"
msgstr ""

#: ../qml/Components/PageBackup.qml:48
msgid "Manage Backups"
msgstr ""

#: ../qml/Components/PageBackup.qml:75
msgid "Export and import keys from a file"
msgstr ""

#: ../qml/Components/PageBackup.qml:88
msgid ""
"Export all keys in one json file. Keep in mind that keys are stored as plain "
"text."
msgstr ""

#: ../qml/Components/PageBackup.qml:96
msgid ""
"It's advisable to encrypt that file. The <a href='https://open-store.io/app/"
"enigma.hummlbach'>Enigma</a> app could help here."
msgstr ""

#: ../qml/Components/PageBackup.qml:106
msgid "Export json file"
msgstr ""

#: ../qml/Components/PageBackup.qml:135
msgid "Import keys from a json file. Select a plain text json file."
msgstr ""

#: ../qml/Components/PageBackup.qml:144
msgid "Import json file"
msgstr ""

#: ../qml/Components/PageBackup.qml:175
msgid ""
"Import keys from a text file containing a list of 'otpauth://' URIs. Select "
"a plain text json file."
msgstr ""

#: ../qml/Components/PageBackup.qml:184
msgid "Import text file"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:45 ../qml/Components/PageImport.qml:45
#: ../qml/Components/PageKeysInfo.qml:48
msgid "%1 Key Information"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:81 ../qml/Components/PageImport.qml:81
#: ../qml/Components/PageKeysInfo.qml:93
msgid "%1 generator: %2 at %32"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:145 ../qml/Components/PageImport.qml:145
#: ../qml/Components/PageKeysInfo.qml:160
msgid "Generate password"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:151 ../qml/Components/PageCheckdB.qml:224
#: ../qml/Components/PageImport.qml:151 ../qml/Components/PageImport.qml:224
#: ../qml/Components/PageKeysInfo.qml:44 ../qml/Components/PageKeysInfo.qml:167
msgid "Counter: %1"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:158 ../qml/Components/PageImport.qml:158
#: ../qml/Components/PageKeysInfo.qml:151
msgid "Copy to clipboard"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:203 ../qml/Components/PageImport.qml:203
#: ../qml/Components/PageKeysInfo.qml:245
msgid "Algorithm: %1"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:209 ../qml/Components/PageImport.qml:209
#: ../qml/Components/PageKeysInfo.qml:251
msgid "Password Length: %1"
msgstr ""

#: ../qml/Components/PageCheckdB.qml:217 ../qml/Components/PageImport.qml:217
#: ../qml/Components/PageKeysInfo.qml:259
msgid "Interval: %1"
msgstr ""

#: ../qml/Components/PageEditAddKey.qml:36
msgid "Key Information"
msgstr ""

#: ../qml/Components/PageHubExport.qml:36
msgid "Export json file to"
msgstr ""

#: ../qml/Components/PageHubImport.qml:35
msgid "Import backup file from"
msgstr ""

#: ../qml/Components/PageHubImport.qml:39
msgid "Back"
msgstr ""

#: ../qml/Components/PageKeysInfo.qml:190
msgid "Complete Key Info"
msgstr ""

#: ../qml/Components/PageKeysResume.qml:42 tfamanager.desktop.in.h:1
msgid "2FA Manager"
msgstr ""

#: ../qml/Components/PageSettings.qml:59
msgid "Theme"
msgstr ""

#: ../qml/Components/PageSettings.qml:85
msgid "Backup and import"
msgstr ""

#: ../qml/Components/PageSettings.qml:105
msgid "Manage Backup"
msgstr ""

#: ../qml/Components/PageSettings.qml:106
msgid "Export, import keys and create a back up file"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:33
msgid "Add Authenticator Key"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:34
msgid "%1 authenticator key"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:43
msgid "Issuer"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:80
msgid "Key"
msgstr ""

#: ../qml/Components/RenderUriPopUp.qml:92
msgid "Cancel"
msgstr ""

#: ../qml/Main.qml:94
msgid "Add Key"
msgstr ""

#: tfamanager.desktop.in.h:2
msgid ""
"2fa;Multi-factor authentication;two factor authenticator;security;passwords;"
msgstr ""
