# ----------------------------------------------------------------------
#
# This file is part of the 2FA Manager App for Ubuntu Touch
# Copyright (C) 2019 Malte Kiefer
# Copyright (C) 2019 Daniel Frost
#
# This App is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This App is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# ----------------------------------------------------------------------
msgid ""
msgstr ""
"Project-Id-Version: tfamanager.cibersheep\n"
"Report-Msgid-Bugs-To: info@cibersheep.com\n"
"POT-Creation-Date: 2024-12-03 21:39+0000\n"
"PO-Revision-Date: 2024-11-22 08:19+0000\n"
"Last-Translator: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>\n"
"Language-Team: German <https://hosted.weblate.org/projects/ubports/"
"tfamanager/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.9-dev\n"

#: ../About.qml.in:37
msgid "About"
msgstr "Über"

#: ../About.qml.in:91
msgid "Version %1. Source %2"
msgstr "Version %1. Quelle %2"

#: ../About.qml.in:100
msgid "Under License %1"
msgstr "Lizenz %1"

#: ../About.qml.in:109
msgid "App used %1 times. Consider donating: %2"
msgstr "App %1 mal verwendet. Überlege zu spenden: %2"

#: ../About.qml.in:147
msgid "App Development"
msgstr "App ENtwicklung"

#: ../About.qml.in:154
msgid "Translate the app"
msgstr "Übersetze die App"

#: ../About.qml.in:160
msgid "Code Used from"
msgstr "Code genutzt von"

#: ../About.qml.in:166 ../About.qml.in:172
msgid "Icons"
msgstr "Icons"

#: ../About.qml.in:167
msgid "App Icon"
msgstr "Anwendungs-Icon"

#: ../About.qml.in:173
msgid "Scan Icon"
msgstr "Scan-Icon"

#: ../qml/Components/ChooserTheme.qml:49
msgid "System Theme"
msgstr "Systemdesign"

#: ../qml/Components/DialogDonate.qml:27
msgid "Consider donating"
msgstr "Überlege zu Spenden"

#: ../qml/Components/DialogDonate.qml:28
msgid "You have been using this app %1 times."
msgstr "Die App wurde bereits %1 mal verwendet."

#: ../qml/Components/DialogDonate.qml:31
msgid "Donate"
msgstr "Spenden"

#: ../qml/Components/DialogDonate.qml:41
#: ../qml/Components/DialogImportResum.qml:32
#: ../qml/Components/HeaderMain.qml:62
msgid "Close"
msgstr "Schließen"

#: ../qml/Components/DialogImportResum.qml:7
msgid "Import Summary"
msgstr "Zusammenfassung importieren"

#: ../qml/Components/DialogImportResum.qml:14
msgid "Keys imported successfully: "
msgstr "Schlüssel erfolgreich importiert: "

#: ../qml/Components/DialogImportResum.qml:20
msgid "Keys skipped: "
msgstr "Übersprungene Schlüssel: "

#: ../qml/Components/DialogImportResum.qml:26
msgid "Keys with errors: "
msgstr "Schlüssel mit Fehlern: "

#: ../qml/Components/EmptyDocument.qml:34
msgid "Empty List"
msgstr "Leere Liste"

#: ../qml/Components/EmptyDocument.qml:35
msgid ""
"Please, scan a Qr code with Barcode Reader app and tap 'Open url' or add "
"info manually in the bottom edge menu"
msgstr ""
"Bitte scanne einen QR-Code mit der Barcode-Reader App und drücke auf \"URL "
"öffnen\" oder füge die Informationen manuell im Menü am unteren Rand hinzu"

#: ../qml/Components/HeaderInfo.qml:39 ../qml/Components/PageEditAddKey.qml:42
msgid "Edit"
msgstr "Ändern"

#: ../qml/Components/HeaderMain.qml:53
msgid "Search key…"
msgstr "Schlüssel suchen…"

#: ../qml/Components/HeaderMain.qml:71 ../qml/Components/PageSettings.qml:33
msgid "Settings"
msgstr "Einstellungen"

#: ../qml/Components/HeaderMain.qml:83
msgid "Search"
msgstr "Suche"

#: ../qml/Components/HeaderSettings.qml:27
msgid "Information"
msgstr "Information"

#: ../qml/Components/KeyToEdit.qml:61
msgid "Description"
msgstr "Beschreibung"

#: ../qml/Components/KeyToEdit.qml:68 ../qml/Components/RenderUriPopUp.qml:38
msgid "Add a description"
msgstr "Eine Beschreibung hinzufügen"

#: ../qml/Components/KeyToEdit.qml:80
msgid "Paste an <i>otpauth://</i> URI"
msgstr "Eine <i>otpauth://</i> URL einfügen"

#: ../qml/Components/KeyToEdit.qml:95
msgid "Paste URI"
msgstr "URL einfügen"

#: ../qml/Components/KeyToEdit.qml:102 ../qml/Components/PageEditAddKey.qml:43
#: ../qml/Components/RenderUriPopUp.qml:86
msgid "Add"
msgstr "Hinzufügen"

#: ../qml/Components/KeyToEdit.qml:118
msgid "Or type the information"
msgstr "Oder gib die Informationen ein"

#: ../qml/Components/KeyToEdit.qml:133
msgid "Type"
msgstr "Typ"

#: ../qml/Components/KeyToEdit.qml:161
msgid "<b>Issuer</b>"
msgstr "<b>Aussteller</b>"

#: ../qml/Components/KeyToEdit.qml:168
msgid "Who is providing the service"
msgstr "Wer bietet diesen Dienst an"

#: ../qml/Components/KeyToEdit.qml:185 ../qml/Components/RenderUriPopUp.qml:48
msgid "User"
msgstr "Nutzer"

#: ../qml/Components/KeyToEdit.qml:192
msgid "Who is using the service"
msgstr "Wer nutzt diesen DIenst"

#: ../qml/Components/KeyToEdit.qml:213 ../qml/Components/RenderUriPopUp.qml:53
msgid "Algorithm"
msgstr "Algorithmus"

#: ../qml/Components/KeyToEdit.qml:242 ../qml/Components/RenderUriPopUp.qml:61
msgid "Password Length"
msgstr "Passwortlänge"

#: ../qml/Components/KeyToEdit.qml:273 ../qml/Components/RenderUriPopUp.qml:68
msgid "Interval"
msgstr "Intervall"

#: ../qml/Components/KeyToEdit.qml:281
msgid "Password life in seconds"
msgstr "Passwort Leben in Sekunden"

#: ../qml/Components/KeyToEdit.qml:300 ../qml/Components/RenderUriPopUp.qml:74
msgid "Counter"
msgstr "Zähler"

#: ../qml/Components/KeyToEdit.qml:307
msgid "Password initial generation"
msgstr "Initial Passwortgenerierung"

#: ../qml/Components/KeyToEdit.qml:318 ../qml/Components/PageCheckdB.qml:188
#: ../qml/Components/PageImport.qml:188 ../qml/Components/PageKeysInfo.qml:230
msgid "Secret Key"
msgstr "Geheimer Schlüssel"

#: ../qml/Components/KeyToEdit.qml:325
msgid "Secret key base"
msgstr "Geheime Schlüsselbasis"

#: ../qml/Components/KeyToEdit.qml:372
msgid ""
"URI format is invalid. The expected format is 'otpauth://totp/[ID]?"
"secret=[CODE]`"
msgstr ""
"Das URL Format ist ungültig. Das erwartete Format ist 'otpauth://totp/[ID]?"
"secret=[CODE]`"

#: ../qml/Components/KeysDelegate.qml:51
msgid "Delete"
msgstr "Löschen"

#: ../qml/Components/KeysDelegate.qml:73
msgid "Copy key"
msgstr "Schlüssel kopieren"

#: ../qml/Components/KeysDelegate.qml:80
msgid "Edit key"
msgstr "Schlüssel ändern"

#: ../qml/Components/KeysDelegate.qml:104
msgid "%1 at %2"
msgstr "%1 bei %2"

#: ../qml/Components/KeysDelegate.qml:105
msgid "%1. %2 at %3"
msgstr "%1. %2 bei %3"

#: ../qml/Components/KeysDelegate.qml:115
msgid "# %1"
msgstr "# %1"

#: ../qml/Components/PageBackup.qml:48
msgid "Manage Backups"
msgstr "Verwalte Sicherungen"

#: ../qml/Components/PageBackup.qml:75
msgid "Export and import keys from a file"
msgstr "Schlüssel mit einer Datei importieren und exportieren"

#: ../qml/Components/PageBackup.qml:88
msgid ""
"Export all keys in one json file. Keep in mind that keys are stored as plain "
"text."
msgstr ""
"Alle Schlüssel in eine json Datei speichern. Bitte beachte, dass die "
"Schlüssel als reiner Text gespeichert werden."

#: ../qml/Components/PageBackup.qml:96
msgid ""
"It's advisable to encrypt that file. The <a href='https://open-store.io/app/"
"enigma.hummlbach'>Enigma</a> app could help here."
msgstr ""
"Es wird empfohlen diese Datei zu verschlüsseln. <a href='https://open-store."
"io/app/enigma.hummlbach'>Enigma</a> App kann dafür hilfreich sein."

#: ../qml/Components/PageBackup.qml:106
msgid "Export json file"
msgstr "Json Datei exportieren"

#: ../qml/Components/PageBackup.qml:135
msgid "Import keys from a json file. Select a plain text json file."
msgstr ""
"Schlüssel von einer json Datei importieren. Wähle eine json Textdatei aus."

#: ../qml/Components/PageBackup.qml:144
msgid "Import json file"
msgstr "Importiere json Datei"

#: ../qml/Components/PageBackup.qml:175
msgid ""
"Import keys from a text file containing a list of 'otpauth://' URIs. Select "
"a plain text json file."
msgstr ""
"Schlüssel aus einer Textdatei mit 'otpauth://' URL's importieren. Wähle eine "
"JSON-Textdatei aus."

#: ../qml/Components/PageBackup.qml:184
msgid "Import text file"
msgstr "Textdatei importieren"

#: ../qml/Components/PageCheckdB.qml:45 ../qml/Components/PageImport.qml:45
#: ../qml/Components/PageKeysInfo.qml:48
msgid "%1 Key Information"
msgstr "%1 Schlüsselinformation"

#: ../qml/Components/PageCheckdB.qml:81 ../qml/Components/PageImport.qml:81
#: ../qml/Components/PageKeysInfo.qml:93
msgid "%1 generator: %2 at %32"
msgstr "%1 Generator: %2 bei %32"

#: ../qml/Components/PageCheckdB.qml:145 ../qml/Components/PageImport.qml:145
#: ../qml/Components/PageKeysInfo.qml:160
msgid "Generate password"
msgstr "Password generieren"

#: ../qml/Components/PageCheckdB.qml:151 ../qml/Components/PageCheckdB.qml:224
#: ../qml/Components/PageImport.qml:151 ../qml/Components/PageImport.qml:224
#: ../qml/Components/PageKeysInfo.qml:44 ../qml/Components/PageKeysInfo.qml:167
msgid "Counter: %1"
msgstr "Zähler: %1"

#: ../qml/Components/PageCheckdB.qml:158 ../qml/Components/PageImport.qml:158
#: ../qml/Components/PageKeysInfo.qml:151
msgid "Copy to clipboard"
msgstr "In die Zwischenablage kopieren"

#: ../qml/Components/PageCheckdB.qml:203 ../qml/Components/PageImport.qml:203
#: ../qml/Components/PageKeysInfo.qml:245
msgid "Algorithm: %1"
msgstr "Algorithmus: %1"

#: ../qml/Components/PageCheckdB.qml:209 ../qml/Components/PageImport.qml:209
#: ../qml/Components/PageKeysInfo.qml:251
msgid "Password Length: %1"
msgstr "Passwortlänge: %1"

#: ../qml/Components/PageCheckdB.qml:217 ../qml/Components/PageImport.qml:217
#: ../qml/Components/PageKeysInfo.qml:259
msgid "Interval: %1"
msgstr "Intervall: %1"

#: ../qml/Components/PageEditAddKey.qml:36
msgid "Key Information"
msgstr "Schlüssel Information"

#: ../qml/Components/PageHubExport.qml:36
msgid "Export json file to"
msgstr "Exportiere json Datei nach"

#: ../qml/Components/PageHubImport.qml:35
msgid "Import backup file from"
msgstr "Sicherung aus Datei importieren"

#: ../qml/Components/PageHubImport.qml:39
msgid "Back"
msgstr "Zurück"

#: ../qml/Components/PageKeysInfo.qml:190
msgid "Complete Key Info"
msgstr "Vollständige Schlüsselinformationen"

#: ../qml/Components/PageKeysResume.qml:42 tfamanager.desktop.in.h:1
msgid "2FA Manager"
msgstr "2FA Manager"

#: ../qml/Components/PageSettings.qml:59
msgid "Theme"
msgstr "Design"

#: ../qml/Components/PageSettings.qml:85
msgid "Backup and import"
msgstr "Sicherung und Import"

#: ../qml/Components/PageSettings.qml:105
msgid "Manage Backup"
msgstr "Sicherung verwalten"

#: ../qml/Components/PageSettings.qml:106
msgid "Export, import keys and create a back up file"
msgstr "Schlüssel exportieren, importieren und Sicherungsdatei anlegen"

#: ../qml/Components/RenderUriPopUp.qml:33
msgid "Add Authenticator Key"
msgstr "Authentifizierungsschlüssel hinzufügen"

#: ../qml/Components/RenderUriPopUp.qml:34
msgid "%1 authenticator key"
msgstr "%1 Authentifizierungsschlüssel"

#: ../qml/Components/RenderUriPopUp.qml:43
msgid "Issuer"
msgstr "Aussteller"

#: ../qml/Components/RenderUriPopUp.qml:80
msgid "Key"
msgstr "Schlüssel"

#: ../qml/Components/RenderUriPopUp.qml:92
msgid "Cancel"
msgstr "Abbrechen"

#: ../qml/Main.qml:94
msgid "Add Key"
msgstr "Schlüssel hinzufügen"

#: tfamanager.desktop.in.h:2
msgid ""
"2fa;Multi-factor authentication;two factor authenticator;security;passwords;"
msgstr ""
"2FA;Multi-Faktor Authentifizierung;Zwei Faktor Authentifizierung;Sicherheit;"
"Passwörter;"

#~ msgid "Received oauth URL"
#~ msgstr "Oauth URL erhalten"

#~ msgid "<b>Theme</b>"
#~ msgstr "<b>Aussehen</b>"

#~ msgid "<b>Type</b>"
#~ msgstr "<b>Typ</b>"

#~ msgid "<b>User</b>"
#~ msgstr "<b>Benutzer</b>"

#~ msgid "<b>Algorithm</b>"
#~ msgstr "<b>Algorithmus</b>"

#~ msgid "<b>Password Length</b>"
#~ msgstr "<b>Passwortlänge</b>"

#~ msgid "<b>Interval</b>"
#~ msgstr "<b>Intervall</b>"

#~ msgid "<b>Counter</b>"
#~ msgstr "<b>Zähler</b>"

#~ msgid "<b>Secret Key</b>"
#~ msgstr "<b>Geheimer Schlüssel</b>"

#~ msgid "<b>Issuer:</b> %1"
#~ msgstr "<b>Aussteller:</b> %1"

#~ msgid "<b>User:</b> %1"
#~ msgstr "<b>Benutzer:</b> %1"

#~ msgid "<b>Algorithm:</b> %1"
#~ msgstr "<b>Algorithmus:</b> %1"

#~ msgid "<b>Password Length:</b> %1"
#~ msgstr "<b>Passwortlänge:</b> %1"

#~ msgid "<b>Interval:</b> %1"
#~ msgstr "<b>Intervall:</b> %1"

#~ msgid "<b>Counter:</b> %1"
#~ msgstr "<b>Zähler:</b> %1"

#~ msgid "<b>Key:</b> %1"
#~ msgstr "<b>Schlüssel:</b> %1"

#~ msgid "6-9 digit password"
#~ msgstr "6-9 Zeichen Passwort"
