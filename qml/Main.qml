/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Qt.labs.settings 1.0
import Lomiri.Components.Popups 1.3
import QtQuick.LocalStorage 2.0
import Lomiri.Content 1.3


//Only need to import OTPAuth once
import "js/otpauth.min.js" as OTPAuth
import "js/db.js" as KeysDB
import "Components"

MainView {
    id: root
    objectName: 'mainView'
    anchorToKeyboard: true

    /* If applicationName starts by a number the UrlDispatcher
     * ignores the incoming link
     * See: https://github.com/ubports/url-dispatcher/issues/7
     */
    applicationName: 'tfamanager.cibersheep'
    automaticOrientation: true

    property string uriToRender
    property bool resumeIsMainPage: true
    property bool isResumeFlicking: false

    //App Color Properties
    readonly property color      mainColor: "#594092"
    readonly property color secondaryColor: "#7b66b5"
    readonly property color highlightColor: "#447b66b5"
    readonly property color     titleColor: LomiriColors.porcelain

    signal initDB()
    signal leftSearchView()

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: theming
        category: "Theming Settings"
        property string themeName: ""

        onThemeNameChanged: {
            setTheme();
            bottomEdge.refresh();
        }
    }

    Settings {
        id: appsettins
        category: "General"

        property real runs: 0
    }

    PageStack {
        id: mainStack
        anchors.fill: parent

        onCurrentPageChanged: {
            console.log("Current PageStack Page: " + currentPage.objectName)
            resumeIsMainPage = currentPage.objectName == "resumePage"
        }
    }

    BottomEdge {
        id: bottomEdge
        enabled: resumeIsMainPage && !isResumeFlicking && !Qt.inputMethod.visible
        height: root.height - Qt.inputMethod.keyboardRectangle.height
        hint.visible: enabled
        hint.text: i18n.tr("Add Key")
        preloadContent: false

        contentUrl: Qt.resolvedUrl("Components/PageEditAddKey.qml")

        //Workaround to refresh BottomEdge when theme changes
        function refresh() {
            commit();
            collapse();
        }
    }

    //Handle incoming otpauth:// url
    Connections {
        target: UriHandler

        onOpened: {
            if (uris.length > 0) {
                for (var i = 0; i < uris.length; i++) {
                    if (uris[i].match(/^otpauth/)) {
                        console.log('URI from UriHandler');
                        renderOTPuri(uris[i]);
                    }
                }
            }
        }
    }

    Connections {
        target: ContentHub

        onImportRequested: {
            console.log('Import from ContentHub');
            for (var i = 0; i < transfer.items.length; i++) {
                renderOTPuri(transfer.items[i].text)
            }

            transfer.finalize()
        }
    }

    Component.onCompleted: {
        //Check if opened the app because we have an incoming uri
        if (Qt.application.arguments && Qt.application.arguments.length > 0) {
            for (var i = 0; i < Qt.application.arguments.length; i++) {

                if (Qt.application.arguments[i].match(/^otpauth/)) {
                    console.log("Received otpauth URIs as Qt.application.arguments")
                    renderOTPuri(Qt.application.arguments[i]);
                }
            }
        }

        setTheme();
        appsettins.runs = appsettins.runs + 1;
        mainStack.push(storedKeysComponent);
    }

    Component {
        //Pop up to add a key
        id: renderUriComponent

        RenderUriPopUp {}
    }

    Component {
        //Delegate of ListItem of codes
        id: keysDelegate

        KeysDelegate {}
    }

    Component {
        //ListItem of codes
        id: storedKeysComponent

        PageKeysResume {
            id: storedKeysPage
            objectName: "resumePage"
        }
    }

    Timer {
        //General ticker. It ticks every second
        id: timer
        running: true
        repeat: true

        property int time

        onTriggered: time = Math.round(new Date().getTime() / 1000.0)
    }

    Timer {
        //Wait 250 miliseconds to load the BottomEdge content. Should improve app start time
        id: botEdgeTimer
        interval: 250
        running: true
        repeat: false

        onTriggered: bottomEdge.preloadContent = true
    }

    function renderOTPuri(uri) {
        /* uriToRender is a general var, otherwise if apps opens from UrlDispatcher,
         * PopUtils.open(renderUriComponent, caller, var) complains about caller not being ready
         * See issue: https://github.com/ubports/ubuntu-ui-toolkit/issues/43
         */
        uriToRender = uri

        PopupUtils.open(renderUriComponent)
    }

    function checkUri(uri) {
        console.log("Checking URI")
        var checkIssuer = findParam("issuer=", uri, "&");
        var checkFirstIssuer;

        checkFirstIssuer = findParam("", uri.slice(15), ":");

        if (!checkFirstIssuer) {
            //We have no issuer and ":[user]"
            console.log("Checking problem with 'issuer'. We have no issuer and ':[user]'")
            return uri.replace("/:","/")
        }

        if (checkIssuer) {
            console.log("Checking problem with issuer (2)...")
            if (checkFirstIssuer !== checkIssuer) {
                console.log(checkFirstIssuer,checkIssuer)
                uri = uri.replace("issuer=" + checkIssuer,"")
                console.log("uri",uri,uri.replace("?&","?"))
                return uri.replace("?&","?")
            }
        }

        return uri;
    }

    function findParam(thisTerm, inText, withSeparator) {
        //Returns xx in [thisTerm]xx[separator or end of inText]
        if (inText.indexOf(withSeparator) == -1) {
            return ""
        } else {
            return inText.slice(inText.indexOf(thisTerm) + thisTerm.length, inText.indexOf(withSeparator) || inText.length);
        }
    }

    function formatCode(code, split) {
        //Format the code to add spaces only for viewing it
        if (split) {
            if (code.length > 6) {
                return code.replace(/(\d{3})(\d{3})(\d)/, '$1 $2 $3');
            } else {
                return code.replace(/(\d{3})(\d{3})/, '$1 $2');
            }
        } else {
            return code.replace(/ /g, "");
        }
    }

    function setTheme() {
        theme.name = theming.themeName
    }
}
