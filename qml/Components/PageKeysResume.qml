/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import QtQuick.LocalStorage 2.0
import "../js/db.js" as KeysDB

Page {
    id: pageKeysResume
    anchors.fill: parent

    signal timerTicked(int time)

    property bool isListBusy: false
    property bool searching: false
    property var filterPattern: new RegExp("")

    //TODO: Do we want to show the hint only when the Flickable is at the top?
    //property alias isAtTop: keysListView.atYBeginning

    header: HeaderMain {
        id: searchHeader
        title: i18n.tr('2FA Manager')
        flickable: keysListView

        onIsSearchingChanged: pageKeysResume.searching = isSearching
        onSearchText: searchKeysModel.search(typedText);
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: keysListView
    }

    ListView {
        id: keysListView
        anchors.fill: parent
        model: searching ? searchKeysModel : storedKeys
        delegate: keysDelegate
    }

    ListModel {
        id: storedKeys

        function populate(data) {
            storedKeys.clear();
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var checkedUri = root.checkUri(data.item(i).keyJSON)

                    try {
                        var parsedKey = OTPAuth.URI.parse(checkedUri);
                        var checkedDesc = data.item(i).description || "";

                        storedKeys.append({
                            desc: checkedDesc,
                            authkey: parsedKey,
                            secret: parsedKey.secret.b32,
                            //We use id to delete the proper db entry
                            id: data.item(i).identifier,
                            url: data.item(i).keyJSON,
                            searchablestring: data.item(i).description + " " + parsedKey.issuer + " " + parsedKey.label
                        });
                    } catch(e) {
                        console.log("Error",e);
                    }
                }
            }
        }
    }

    SortFilterModel {
        id: searchKeysModel
        model: storedKeys

        sort {
            property: "desc"
            order: Qt.DescendingOrder
        }

        filter {
            property: "searchablestring"
            pattern: pageKeysResume.filterPattern
        }

        function search(text) {
            pageKeysResume.filterPattern = text ? new RegExp(text, 'i') : new RegExp("")
        }
    }

    Loader {
        id: emptyStateLoader
        anchors.fill: parent
        active: storedKeys.count === 0 && !isListBusy
        source: Qt.resolvedUrl("EmptyDocument.qml")
    }

    Component.onCompleted: {
        initialize();

        if (appsettins.runs == 50 || appsettins.runs == 100 || appsettins.runs == 200) {
            PopupUtils.open(Qt.resolvedUrl("DialogDonate.qml"), pageKeysResume, { 'runs': appsettins.runs } )
        }
    }

    Connections {
        target: root

        onInitDB: {
            console.log('Received initDB signal');
            initialize();
        }

        //When tapping an element in the ListView, close the search view
        onLeftSearchView: {
            //Signal to clear the text in the SearchHeader
            searchHeader.closeHeader()
        }
    }

    function initialize() {
        isListBusy = true;
        var allKeys = KeysDB.getKeys();
        storedKeys.populate(allKeys.rows);
        isListBusy = false;
    }
}
