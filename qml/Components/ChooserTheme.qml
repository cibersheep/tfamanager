/*
 * This file is based on work taken from Ubuntu Weather app as on September 2019. Copyright (C) 2019 UBports
 * Copyright (C) 2019 Joan Cibersheep
 *
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0

Item {
    height: themeSetting.height

    property string selectedTheme: theming.themeName == ""
        ? "System Theme"
        : theming.themeName == "Lomiri.Components.Themes.Ambiance"
            ? "Ambiance"
            : "Suru Dark"

    property string themeToBe

    ListModel {
        id: themeModel

        function initialize() {
            themeModel.append([
                {   "text": "Ambiance",
                    "theme": "Lomiri.Components.Themes.Ambiance"
                },
                {   "text": "Suru Dark",
                    "theme": "Lomiri.Components.Themes.SuruDark"
                },
                {   "text": i18n.tr("System Theme"),
                    "theme": ""
                }
            ]);
        }

        Component.onCompleted: initialize()
    }

    ExpandableListItem {
        id: themeSetting
        listViewHeight: themeModel.count * units.gu(6.1)
        model: themeModel
        title.text: selectedTheme

        delegate: StandardListItem {
            id: rrr
            title.text: model.text
            icon.name: "ok"
            icon.visible: selectedTheme === model.text

            onClicked: {
                themeToBe = model.theme;
                selectedTheme = model.text;
                themeSetting.toggleExpansion();
                themeTimer.start()
            }
        }
    }

    Timer {
       id: themeTimer
       //intervall needs to be a minimum of 200, otherwise toggleExpansion is not entirely finished
       interval: 175
       running: false
       repeat: false

       onTriggered: {
          theming.themeName = themeToBe
       }
    }
}
