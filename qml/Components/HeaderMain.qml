/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

HeaderBase {
    signal searchText(string typedText)

    property bool isSearching: false

    contents: Row {
        anchors.centerIn: parent
        width: parent.width
        spacing: units.gu(1.5)

        Label {
            visible: !isSearching
            text: header.title
            textSize: Label.Large
            color: root.titleColor
        }

        TextField {
            id: searchField
            visible: isSearching
            width: parent.width

            // Disable predictive text
            inputMethodHints: Qt.ImhNoPredictiveText

            primaryItem: Icon {
                width: units.gu(2); height: width
                name: "find"
            }

            placeholderText: i18n.tr("Search key…")

            onTextChanged: searchText(text);
        }
    }

    leadingActionBar.actions: Action {
        iconName: "close"
        visible: isSearching
        text: i18n.tr("Close")

        onTriggered: closeHeader();
    }

    trailingActionBar {
        actions: [
            Action {
                iconName: "settings"
                text: i18n.tr("Settings")

                onTriggered: {
                    if (isSearching) {
                        closeHeader();
                    }

                    mainStack.push(Qt.resolvedUrl("PageSettings.qml"));
                }
            },
            Action {
                iconName: "search"
                text: i18n.tr("Search")
                visible: !isSearching

                onTriggered: {
                    isSearching = true;
                    searchField.focus = true;
                }
            }
        ]
    }

    function closeHeader() {
        searchField.text = "";
        isSearching = false;
        searchField.focus = false;
    }
}
