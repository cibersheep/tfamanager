/*
 * This file is based on work taken from Costales' uNav app as on September 2019.
 * Copyright (C) 2016 Nekhelesh Ramananthan https://launchpad.net/~nik9077
 *
 * Copyright (C) 2019 Joan Cibersheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

ListItem {
    id: headerListItem

    property string title
    height: Math.max(units.gu(8), textLabel.height + units.gu(2))
    width: parent.width

    Label {
        id: textLabel
        width: parent.width - units.gu(4)

        anchors {
            bottom: parent.bottom
            bottomMargin: units.gu(2)
            left: parent.left
            leftMargin: units.gu(2)
        }

        text: title
        wrapMode: Text.WordWrap
        font.bold: true
        color: mainColor
    }

    divider.visible: false
}
