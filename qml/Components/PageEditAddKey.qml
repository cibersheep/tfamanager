/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.LocalStorage 2.0
import "../js/db.js" as KeysDB

Page {
    id: editAddKeyPage

    property var uriToEdit
    property alias descriptionToEdit: info.userDesc
    property bool editMode: false
    property int idToUpdate

    header: HeaderBase {
        id: pageHeader
        title: i18n.tr("Key Information")

        trailingActionBar {
        actions: Action {
                iconName: "ok"
                text: editMode
                    ? i18n.tr("Edit")
                    : i18n.tr("Add")

                enabled: !info.isInfoEmpty
                onTriggered: addKey()
            }
        }
    }

    Flickable {
        id: editAddFlickable
        width: parent.width
        height: parent.height
        clip: true
        contentHeight: info.height
        topMargin: pageHeader.height + units.gu(2)
        bottomMargin: Qt.inputMethod.visible && !editMode
            ? Qt.inputMethod.keyboardRectangle.height + units.gu(2)
            : units.gu(4)

        KeyToEdit {
            id: info
            width: parent.width * .85
            anchors.horizontalCenter: parent.horizontalCenter
            editMode: editAddKeyPage.editMode
        }
    }

    function addKey() {
        var editedKey = info.checkFields();

        if (info.isTOTP) {
            editedKey = new OTPAuth.TOTP(editedKey);
        } else {
            editedKey = new OTPAuth.HOTP(editedKey);
        }

        if (editMode) {
            KeysDB.updateStoredKey(idToUpdate, Date(), editedKey.toString(), info.userDesc);
        } else {
            KeysDB.storeKey(Date(), editedKey.toString(), info.userDesc);
        }

        root.initDB();
        closingPop();
    }

    function closingPop() {
        if (editMode) {
            mainStack.pop();
        } else {
            bottomEdge.collapse();
        }
    }

    Component.onCompleted: {
        if (editMode) {
            try {
                info.isTOTP = uriToEdit.match("totp") !== null;
                info.uriJson = OTPAuth.URI.parse(uriToEdit);
            } catch(e) {
                console.log("Error",e);
            }
        }
    }
}
