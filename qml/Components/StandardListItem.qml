/*
 * This file is based on work taken from Ubuntu Weather app as on September 2019. Copyright (C) 2019 UBports
 * Copyright (C) 2019 Joan Cibersheep
 *
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

ListItem {
    id: listItem

    property alias title: listitemlayout.title
    property alias icon: _icon

    // For autopilot
    readonly property bool iconVisible: icon.visible
    readonly property string titleValue: title.text

    height: listitemlayout.height + divider.height
    divider.visible: false

    ListItemLayout {
        id: listitemlayout

        title.text: ""

        Icon {
            id: _icon
            height: units.gu(2); width: height
            name: "go-next"
            SlotsLayout.position: SlotsLayout.Last
        }
    }
}
