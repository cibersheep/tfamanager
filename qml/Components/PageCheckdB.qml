/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import QtQuick.LocalStorage 2.0
//import "../js/otpauth.min.js" as OTPAuth
import "../js/db.js" as KeysDB

Page {
    id: keyInfoPage
    objectName: "keyInfoPage"
    anchors.fill: parent

    property var key
    property int id
    property int reminingTime
    property string secret
    property string url
    property string desc

    property bool isTOTP
    property string authType

    header: HeaderInfo {
        id: pageHeader
        title: i18n.tr("%1 Key Information").arg(desc)
        flickable: mainFlickable
    }

    ProgressBar {
        id: bar
        visible: isTOTP
        width: parent.width
        anchors.top: header.bottom

        maximumValue: key.period || 0
        minimumValue: 0
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: mainFlickable
    }

    Flickable {
        id: mainFlickable
        width: parent.width
        height: parent.height
        contentHeight: mainColumn.height
        topMargin: units.gu(2)
        bottomMargin: units.gu(4)

        Column {
            id: mainColumn
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: units.gu(1)
            spacing: units.gu(2)

            Label {
                text: i18n.tr("%1 generator: %2 at %32").arg(authType).arg(key.label).arg(key.issuer)
                textSize: Label.Small
                anchors.horizontalCenter: parent.horizontalCenter
            }

            LomiriShape {
                id: contentShape
                anchors.horizontalCenter: parent.horizontalCenter
                width: Math.min(parent.width - units.gu(8), units.gu(68))
                height: generatedKey.height + units.gu(8)
                aspect: LomiriShape.Flat
                color: root.highlightColor

                onWidthChanged: generatedKey.adjustTextSize()

                Label {
                    id: generatedKey
                    anchors.centerIn: parent

                    text: generateKey(isTOTP)

                    //TODO: This might be too slow for what it does. Consider
                    //From Dialer App
                    property double maximumFontSize: units.dp(35)
                    readonly property double minimumFontSize: FontUtils.sizeToPixels("medium")
                    property bool __adjusting: false

                    fontSizeMode: Text.HorizontalFit

                    function adjustTextSize(){
                        // avoid infinite recursion here
                        if (__adjusting) return;

                        __adjusting = true;

                        // start by resetting the font size to discover the scale that should be used
                        font.pixelSize = maximumFontSize

                        // check if it really needs to be scaled
                        if (contentShape.width < generatedKey.width) {
                            var factor = contentShape.width / generatedKey.width;
                            font.pixelSize = Math.max(font.pixelSize * factor, minimumFontSize);
                        }
                        __adjusting = false
                    }

                    Connections {
                        target: units
                        onGridUnitChanged: adjustTextSize()
                    }

                    Component.onCompleted: adjustTextSize()
                }
            }

            Row {
                width: parent.width - units.gu(8)
                spacing: units.gu(4)
                anchors.horizontalCenter: parent.horizontalCenter

                Button {
                    id: regenBut
                    visible: !keyPeriod.visible
                    width: parent.width/2 - units.gu(2)
                    text: i18n.tr("Generate password")
                    onClicked: {
                        var difference =  1;
                        url = KeysDB.updateCounter(id, difference);
                        keyInfoPage.key = OTPAuth.URI.parse(url);
                        generatedKey.text = generateKey(isTOTP);
                        counterHtop.text = i18n.tr("Counter: %1").arg(key.counter)
                        root.initDB();
                    }
                }

                Button {
                    width: regenBut.visible ? parent.width/2 - units.gu(2) : parent.width
                    text: i18n.tr("Copy to clipboard")
                    onClicked: Clipboard.push(generatedKey.text)
                    color: theme.palette.normal.activity
                }
            }

            Rectangle {
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            LomiriShape {
                anchors.horizontalCenter: parent.horizontalCenter
                width: units.gu(21)
                height: width
                sourceFillMode: LomiriShape.Pad
                aspect: LomiriShape.Flat
                backgroundColor: "#ffffff"

                source: QRCode {
                    id: qr
                    width: parent.width - units.gu(4)
                    height: width
                    anchors.bottom: parent.bottom
                    value : url
                }
            }

            Text {
                text: i18n.tr("Secret Key")
                anchors.horizontalCenter: parent.horizontalCenter
                height: units.gu(5)
                verticalAlignment: Text.AlignBottom
                color: theme.palette.normal.baseText
            }

            Label {
                text: secret
                textSize: Label.XSmall
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.baseText
            }

            Text {
                text: i18n.tr("Algorithm: %1").arg(key.algorithm)
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.baseText
            }

            Text {
                text: i18n.tr("Password Length: %1").arg(key.digits)
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.baseText
            }

            Text {
                id: keyPeriod
                visible: isTOTP
                text: i18n.tr("Interval: %1").arg(key.period)
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.baseText
            }

            Text {
                id: counterHtop
                text: i18n.tr("Counter: %1").arg(key.counter)
                visible: !keyPeriod.visible
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.baseText
            }
        }
    }

    //Let's connect with the main ticker
    Connections {
        target: timer

        onTriggered: {
            generatedKey.text = generateKey(isTOTP)

            bar.value = timer.time % key.period
        }
    }

    Component.onCompleted: {
        isTOTP = key.counter == undefined;
        bar.value = reminingTime;

        authType = isTOTP
            ? "TOTP"
            : "HTOP"
    }

    function generateKey(boolVar) {
        return boolVar
            ? OTPAuth.TOTP.generate(key)
            : OTPAuth.HOTP.generate(key)
    }
}
