/*
 * This file is based on work taken from Ubuntu Weather app as on September 2019. Copyright (C) 2019 UBports
 * Copyright (C) 2019 Joan Cibersheep
 *
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

Item {
    height: algSetting.height

    property string selectedAlg: "SHA1"

    ListModel {
        id: algModel
        Component.onCompleted: initialize()

        function initialize() {
            algModel.append({"text": "SHA1"})
            algModel.append({"text": "SHA256"})
            algModel.append({"text": "SHA512"})
        }
    }

    ExpandableListItem {
        id: algSetting
        listViewHeight: algModel.count*units.gu(6.1)
        model: algModel
        title.text: selectedAlg

        delegate: StandardListItem {
            title.text: model.text
            icon.name: "ok"
            icon.visible: selectedAlg === model.text
            onClicked: {
                selectedAlg = model.text;
                algSetting.toggleExpansion();
            }
        }
    }
}
