# 2FA Manager

Two Factor Authenticator manager app for Ubuntu Touch.

[![OpenStore](https://open-store.io/badges/ca.svg)](https://open-store.io/app/tfamanager.cibersheep)

## Building

Clone the repo: `git clone git@gitlab.com:cibersheep/tfamanager.git`

Build with [clickable](https://clickable-ut.dev):
- Run on the desktop: `clickable desktop`
- Any architecture device attached to the computer: `clickable`

## Translating

You can use: https://hosted.weblate.org/projects/ubports/tfamanager/ to add or modify your tranlation.

## Aknoledgements

UBports kindly enabled a space on their hosted weblate. Thank you.
